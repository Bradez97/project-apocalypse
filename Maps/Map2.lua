  local tileString = [[
~~~~~~~~~~~~~~~~~~
~%-----!~%------!~
~<+    *~*     =>~
~<     *~*      >~
~<##            >~
~<     *~*      >~
~<     *~*      >~
~<            ##>~
~<    ^*~*      >~
~<=    *~*^    @>~
~.?????,~.??????,~
~~~~~~~~~~~~~~~~~~
]] 

  local quadInfo = 
  {
  --{Char, x, y} 
    {'+',0, 0},   --Blu Fort
    {'=',60,0},   --Grey Fort
    {'@',120,0},  --Red Fort
    {'.',180,0},  --Isle BotLeft
    {',',240,0},  --Isle BotRight
    {'?',300,0},  --Isle Bot
    {'>',360,0},  --Isle Right
    {'<',420,0},  --Isle Left
    {'%',480,0},  --Isle TopLeft
    {'!',540,0},  --Isle TopRight
    {'-',600,0},  --Isle Top
    {'*',660, 0},  --Marsh
    {'^',720, 0}, --Mountain
    {'~',780, 0},--Ocean
    {'#',840, 0},--Forest
    {' ',900, 0} --Plain
  }
  
  newMap(60,60,'sprites/spritesheet.png',tileString,quadInfo)