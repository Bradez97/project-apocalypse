function love.conf(c)
  c.title = "Project Apocalypse"
  local window = c.screen or c.window -- love 0.9 renamed "screen" to "window"
  window.width = 1080
  window.height = 720
end